unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AppEvnts, ComCtrls, Menus, XPMan, ShellAPI;

const playercount=1;


type
   Location = record
left_wall, up_wall : Boolean;
    end;
 Maze = array of array of Location;
    TMarkRect = record
    x,y:Integer;
    end;
    
  TForm1 = class(TForm)
    BackBuffer: TImage;
    Screen: TImage;
    scrlbx1: TScrollBox;
    mm1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    dlgOpen1: TOpenDialog;
    dlgSave1: TSaveDialog;
    xpmnfst1: TXPManifest;
   procedure LoadMaze(var TheMaze : Maze; FileName : string);
   procedure SaveMaze(TheMaze : Maze; FileName : string);
   procedure ShowMaze(TheMaze : Maze);
   function PrimGenerateMaze(Width, Height : Integer) : Maze;
   function KruskalGenerateMaze(Width, Height : Integer) : Maze;
   procedure RecursiveSolve(TheMaze : Maze; xs, ys, xf, yf : Integer);
   function WaveTracingSolve(TheMaze : Maze; xs, ys, xf, yf : Integer):Boolean;
   function Rectangel(x,y:integer):Tmarkrect;
   procedure RectangelFree(x,y:integer);
   function CanGo(x, y, dx, dy : Integer) : Boolean;
   function MoveRect(dx,dy:integer;markrect:TMarkRect):TMarkRect;
   procedure NewGame;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure top(Sender: TObject);
    procedure right(Sender: TObject);
    procedure left(Sender: TObject);
    procedure bottom(Sender: TObject);
    procedure ScreenMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure N14Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N17Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  TheMaze: Maze;
  markrectangel:TMarkRect;
  markerUsers:array of array of Integer;
  startx,starty,finishx,finishy,k:Integer;
  ColorAr: array[0..2 - 1] of TColor;

implementation

uses Unit2;

{$R *.dfm}
{ TForm1 }

procedure TForm1.LoadMaze(var TheMaze: Maze; FileName: string);
var f	: TextFile;	{ ���� � ��������� ��������� }
Height, Width :	Integer;	{ ������ � ������ ��������� }
x, y	: Integer;	{ ������� ������� }
lw, uw	: Integer;	{ ��������� ���������� }

begin
   try
  AssignFile(f, FileName);	{ ������� ���� }
  Reset(f);

  ReadLn(f, Width, Height);	{ ��������� ������ � ������ }
  SetLength(TheMaze, Width + 1, Height + 1);	{ �������� ������ ��������� }

  for	y := 0 to Height do	{ ���� �� ���� �������� }
  for x := 0 to Width do
    if (y = Height) or (x = Width) then	{ ���� ������� - ��������� }
    begin
    TheMaze[x, y].left_wall := true;	{ ��� ����� ���������� }
    TheMaze[x, y].up_wall := true;
    end
    else
    begin	{ ����� ��������� }
      ReadLn(f, uw, lw);	{ �� ����� }
    TheMaze[x, y].left_wall := Boolean(lw);	{ ����������� ����� }
    TheMaze[x, y].up_wall := Boolean(uw);	{ ����� ���� �������� }
    end;	{ � ���� Boolean }
  except
    ShowMessage('������ ������ �����');
    end;
CloseFile(f);
end;


procedure TForm1.SaveMaze(TheMaze : Maze; FileName : string);
var	f	: TextFile;	{ ���� � ��������� ��������� }
	Height, Width	: Integer;	{ ������ � ������ }
	x, y	: Integer;	{ ���������� ������� ������� }
begin
 try
	AssignFile(f, FileName);	{ ������� ���� }
	Rewrite(f);	{ ��� ������ }
	Height := High(TheMaze[0]);	{ ���������� ������ }
	Width := High(TheMaze);	{ � ������ ��������� }
	WriteLn(f, Width, ' ', Height);	{ ������ � ���� ������ � ������ }
  for	y := 0 to Height - 1 do 	{ ������ ������ ������� }
  for	x := 0 to Width - 1 do
    WriteLn(f, Integer(TheMaze[x, y].up_wall), ' ',
    Integer(TheMaze[x, y].left_wall));
  except
    ShowMessage('������ ������ �����');
    end;
CloseFile(f); 	{ ������� ���� }
end;

{����������� ����� ���������}
procedure TForm1.RecursiveSolve(TheMaze : Maze; xs, ys, xf, yf : Integer);
var
Visited : array of array of Boolean; { ����� ���������� ������� }
x, y, xc, yc : Integer;
i : Integer;
Path : array of TPoint;
Height, Width : Integer;
const
dx : array[1..4] of Integer = (1, 0, -1, 0); { �������� }
dy : array[1..4] of Integer = (0, -1, 0, 1);

{ ����� �������� ������� �� ����� (x, y) }
function Solve(x, y, depth : Integer) : Boolean;
var i : Integer;
begin
Visited[x, y] := true;
Path[depth] := Point(x, y);
Path[depth + 1] := Point(-1, -1); { ������� ����� �������� }

if (x = xf) and (y = yf) then { ���� �������� ������� ������� }
  begin
  Solve := true; { ����� ��������� }
  Exit;
  end;

for i := 1 to 4 do
  { ���� ������� ��������, ���� �� ��� }
  if CanGo(x, y, dx[i], dy[i]) and
  not Visited[x + dx[i], y + dy[i]] then
  if Solve(x + dx[i], y + dy[i], depth + 1) then
  begin
  Solve := true;
  Exit; { ����� ��������� }
  end;

Visited[x, y] := false;
Solve := false; { ������� �� ������� }
end;

begin
Width := High(TheMaze);
Height := High(TheMaze[0]);
SetLength(Path, Height * Width + 1);
SetLength(Visited, Width, Height);

for x := 0 to Width - 1 do
  for y := 0 to Height - 1 do
  Visited[x, y] := false;

if Solve(xs, ys, 0) then { ���� ������� �������, ������ ��� }
  begin
  i := 0;
  while not ((Path[i].X = -1) and (Path[i].Y = -1)) do
    begin
    xc := strtoint(form2.edt3.text) * (2 * Path[i].X + 1) div 2;
    yc := strtoint(form2.edt3.text) * (2 * Path[i].Y + 1) div 2;
    Form1.Screen.Canvas.Ellipse(xc - 5, yc - 5, xc + 5, yc + 5);
    i := i + 1;
    end;
  end;
end;

function TForm1.WaveTracingSolve(TheMaze : Maze; xs, ys, xf, yf : Integer):Boolean;
var Mark : array of array of Integer; { ����� ������� }
x, y, xc, yc : Integer;
N, i : Integer;
Height, Width : Integer;
const dx : array[1..4] of Integer = (1, 0, -1, 0); { �������� }
dy : array[1..4] of Integer = (0, -1, 0, 1);

function CanGo(x, y, dx, dy : Integer) : Boolean;
begin
if dx = -1 then CanGo := not TheMaze[x, y].left_wall
  else if dx = 1 then CanGo := not TheMaze[x + 1, y].left_wall
  else if dy = -1 then CanGo := not TheMaze[x, y].up_wall
  else CanGo := not TheMaze[x, y + 1].up_wall;
end;

function Solve : Boolean; { ����� ������� }
var i, N, x, y : Integer;
NoSolution : Boolean;
begin
N := 1;
  repeat
  NoSolution := true; { ��������, ��� ������� ��� }
  for x := 0 to Width - 1 do
  for y := 0 to Height - 1 do
  if Mark[x, y] = N then { ����� �������, ���������� ������ N }
  for i := 1 to 4 do { �������� �������� ������� }
  if CanGo(x, y, dx[i], dy[i]) and
  (Mark[x + dx[i], y + dy[i]] = 0) then
    begin { ������� �������� � �������� ����� }
    NoSolution := false; { ���� ���� ����� ������� }
    { �������� �������� ������� ������ N +1 }
    Mark[x + dx[i], y + dy[i]] := N + 1;
    if (x + dx[i] = xf) and (y + dy[i] = yf) then
      begin
      Solve := true;
      Exit; { ����� ��������� }
      end;
    end;
  N := N + 1;
  until NoSolution;
Solve := false; { ������� �� ������� }
end;

begin
  result:=False;
Width := High(TheMaze);
Height := High(TheMaze[0]);
SetLength(Mark, Width, Height);

for x := 0 to Width - 1 do
  for y := 0 to Height - 1 do
  Mark[x, y] := 0;

Mark[xs, ys] := 1; { ��������� ������� ������������� ������� }
if Solve then { ���� ������� �������, ������ ��� }
  begin
    result:=true;
  x := xf; y := yf;
  for N := Mark[xf, yf] downto 1 do
    begin
    xc := strtoint(form2.edt3.text) * (2 * x + 1) div 2;
    yc := strtoint(form2.edt3.text) * (2 * y + 1) div 2;
    Form1.Screen.Canvas.Ellipse(xc - 5, yc - 5, xc + 5, yc + 5);
    for i := 1 to 4 do
      if CanGo(x, y, dx[i], dy[i]) and
      (Mark[x + dx[i], y + dy[i]] = N - 1) then
      begin
      x := x + dx[i]; { ���� ��������� ������� �������� }
      y := y + dy[i];
      Break;
      end;
    end;
  end;
end;

procedure TForm1.ShowMaze(TheMaze : Maze); { ���������� �������� }
var x, y ,xc,yc: Integer;
Height, Width : Integer; { ������ � ������ ��������� }
begin
Width := High(TheMaze); { ���������� ������ � ������ }
Height := High(TheMaze[0]);
with Form1.BackBuffer.Canvas do
  begin { ������� ������ }
  FillRect(Rect(0, 0, Form1.BackBuffer.Width, Form1.BackBuffer.Height));
  for x := 0 to Width - 1 do
    for y := 0 to Height - 1 do
    begin
    { ���� � ������� ���� ������� ����� }
      if TheMaze[x, y].up_wall then
      begin
      MoveTo(x * strtoint(form2.edt3.text), y * strtoint(form2.edt3.text)); { ������ �� }
      LineTo((x + 1) * strtoint(form2.edt3.text), y * strtoint(form2.edt3.text));
      end;

    { ���� � ������� ���� ����� ����� }
      if TheMaze[x, y].left_wall then
      begin
      MoveTo(x * strtoint(form2.edt3.text), y * strtoint(form2.edt3.text)); { ������ � �� }
      LineTo(x * strtoint(form2.edt3.text), (y + 1) * strtoint(form2.edt3.text));
      end;

    end;

  MoveTo(0, Height * strtoint(form2.edt3.text)); { ������ ����� ����� � }
  LineTo(Width * strtoint(form2.edt3.text), Height * strtoint(form2.edt3.text)); { ������ �� ��������� }
  LineTo(Width * strtoint(form2.edt3.text), 0);


//    xc := strtoint(form2.edt3.text) * (2 * (strtoint(form2.edit1.text)-1) + 1) div 2;
//    yc := strtoint(form2.edt3.text) * (2 * (strtoint(form2.edit2.text)-1) + 1) div 2;
//   Pen.Color:=clRed;
//   Brush.Color:=clRed;
//   Ellipse(xc - 5, yc - 5, xc + 5, yc + 5);
//   Brush.Color:=clWindow;
//   Pen.Color:=clBlack;
  end;

{ ���������� ��������� �� �������� ������ }
Form1.Screen.Canvas.CopyRect(Rect(0, 0, Form1.Screen.Width,
Form1.Screen.Height), Form1.BackBuffer.Canvas,
Rect(0, 0, Form1.Screen.Width, Form1.Screen.Height));
end;

{��������� ��������� �� ��������� �����}
function Tform1.PrimGenerateMaze(Width, Height : Integer) : Maze;
type AttrType = (Inside, Outside, Border); { ��� "������� �������" }
var
TheMaze : Maze; { ��� �������� }
x, y, i : Integer;
xc, yc : Integer;
xloc, yloc : Integer;
Attribute : array of array of AttrType; { ����� ��������� }
IsEnd : Boolean;
counter : Integer;
const dx : array[1..4] of Integer = (1, 0, -1, 0); { �������� }
dy : array[1..4] of Integer = (0, -1, 0, 1);
label ExitFor1, ExitFor2, ExitFor3;

procedure BreakWall(x, y, dx, dy : Integer); { ��������� ����� }
begin { ����� ��������� }
  if dx = -1 then TheMaze[x, y].left_wall := false
  else if dx = 1 then TheMaze[x + 1, y].left_wall := false
  else if dy = -1 then TheMaze[x, y].up_wall := false
  else TheMaze[x, y + 1].up_wall := false;
end;

begin
scrlbx1.HorzScrollBar.Range:=Width*strtoint(form2.edt3.text)+1;
scrlbx1.VertScrollBar.Range:=Height*strtoint(form2.edt3.text)+1;
SetLength(Attribute, Width, Height); { ��������� ������ ��� ��������� }
SetLength(TheMaze, Width + 1, Height + 1); { �������� ������ ��������� }

for x := 0 to Width - 1 do { ���������� ��� �������� }
  for y := 0 to Height - 1 do { ����� Outside }
  Attribute[x, y] := Outside;

for y := 0 to Height do { ��� ����� ���������� }
  for x := 0 to Width do { ���������� }
  begin
  TheMaze[x, y].left_wall := true;
  TheMaze[x, y].up_wall := true;
  end;

Randomize;
x := Random(Width); { �������� ��������� ������� }
y := Random(Height);
Attribute[x, y] := Inside; { � ����������� �� ������� Inside }

for i := 1 to 4 do { ���� �� ������� ����������� }
  begin { ������� Border }
  xc := x + dx[i];
  yc := y + dy[i];
  if (xc >= 0) and (yc >= 0) and (xc < Width) and (yc < Height) then
  Attribute[xc, yc] := Border;
  end;

  repeat { ������� ���� }
  IsEnd := true;
  counter := 0;
  for x := 0 to Width - 1 do { ������������ ���������� }
    for y := 0 to Height - 1 do { ������� � ��������� Border }
    if Attribute[x, y] = Border then counter := counter + 1;

  counter := Random(counter) + 1; { �������� �� ��� }
  for x := 0 to Width - 1 do { ���� ��������� }
    for y := 0 to Height - 1 do
    if Attribute[x, y] = Border then
    begin
    counter := counter - 1;
    if counter = 0 then
      begin
      xloc := x; { xloc, yloc - �� ���������� }
      yloc := y;
      goto ExitFor1; { ����� �� ����� }
      end;
    end;
  ExitFor1:
  Attribute[xloc, yloc] := Inside; { ��������� �� ������� Inside }

  counter := 0;
  for i := 1 to 4 do
    begin
    xc := xloc + dx[i];
    yc := yloc + dy[i];
      if (xc >= 0) and (yc >= 0) and (xc < Width) and (yc < Height) then
      begin { ���������� ���������� ������� � ��������� Inside }
      if Attribute[xc, yc] = Inside then counter := counter + 1;
      if Attribute[xc, yc] = Outside then { �������� �������� � }
      Attribute[xc, yc] := Border; { Outside �� Border }
      end;
    end;

  counter := Random(counter) + 1; { ������� ��������� Inside-������� }
  for i := 1 to 4 do
    begin
    xc := xloc + dx[i];
    yc := yloc + dy[i];
    if (xc >= 0) and (yc >= 0) and (xc < Width) and (yc < Height)
      and (Attribute[xc, yc] = Inside) then
      begin
      counter := counter - 1;
      if counter = 0 then { ��������� ����� ����� ��� � }
        begin { ������� �������� }
        BreakWall(xloc, yloc, dx[i], dy[i]);
        goto ExitFor2;
        end;
      end;
    end;
  ExitFor2:
  for x := 0 to Width - 1 do { ����������, ���� �� }
    for y := 0 to Height - 1 do { ���� ���� ������� � }
    if Attribute[x, y] = Border then { ��������� Border }
    begin
    IsEnd := false; { ���� ��, ���������� }
    goto ExitFor3; { ��������� �������� }
    end;
  ExitFor3:
  ShowMaze(TheMaze); { ���������� ������� ��������� }
  Application.ProcessMessages;
  until IsEnd;
PrimGenerateMaze := TheMaze;
end;


function TForm1.KruskalGenerateMaze(Width, Height : Integer) : Maze;
type
Wall = record
x, y, dx, dy : Integer;
end;
var
TheMaze : Maze;
Walls : array of Wall;
Temp : array of Real;
i, j,RHeight,RWidth : Integer;
tempw : Wall;
tempr : Real;
CurWall : Wall;
locations : Integer;
counter : Integer;
const
dx : array[1..4] of Integer = (1, 0, -1, 0);
dy : array[1..4] of Integer = (0, -1, 0, 1);

procedure BreakWall(x, y, dx, dy : Integer);
begin
if dx = -1 then TheMaze[x, y].left_wall := false
  else if dx = 1 then TheMaze[x + 1, y].left_wall := false
  else if dy = -1 then TheMaze[x, y].up_wall := false
  else TheMaze[x, y + 1].up_wall := false;
end;


function IsConnected(xs, ys, xf, yf : Integer) : Boolean;
begin
try
 result:=WaveTracingSolve(TheMaze,xs,ys,xf,yf);
 except
         Showmessage(inttostr(xs));
 end;
end;



begin
  RHeight:=strtoint(Form2.edit1.text);
  RWidth:=strtoint(Form2.edit2.text);
  if rWidth >= rHeight then begin
scrlbx1.HorzScrollBar.Range:=rWidth*strtoint(form2.edt3.text)+1;
scrlbx1.VertScrollBar.Range:=rWidth*strtoint(form2.edt3.text)+1;
end else begin
  scrlbx1.HorzScrollBar.Range:=rHeight*strtoint(form2.edt3.text)+1;
scrlbx1.VertScrollBar.Range:=rHeight*strtoint(form2.edt3.text)+1;
end;
{Screen.Width:=Width*strtoint(form2.edt3.text)+1;
Screen.Height:=Height*strtoint(form2.edt3.text)+1;
BackBuffer.Width:=Width*strtoint(form2.edt3.text)+1;
BackBuffer.Height:=Height*strtoint(form2.edt3.text)+1;       }

    SetLength(Walls, (Width - 1) * Height + (Height - 1) * Width);
    SetLength(Temp, (Width - 1) * Height + (Height - 1) * Width); 
    SetLength(TheMaze, Width + 1, Height + 1);

    for i := 0 to Width do
    for j := 0 to Height do
    begin
    TheMaze[i, j].left_wall := true;
    TheMaze[i, j].up_wall := true;
    end;

    Randomize; 
    for i := 0 to (Width - 1) * Height + (Height - 1) * Width - 1 do 
    Temp[i] := Random;

    counter := 0;
    for i := 1 to Width - 1 do 
    for j := 0 to Height - 1 do 
    begin
    Walls[counter].x := i;
    Walls[counter].y := j;
    Walls[counter].dx := -1;
    Walls[counter].dy := 0;
    counter := counter + 1; 
    end;
    for i := 0 to Width - 1 do 
    for j := 1 to Height - 1 do
    begin
    Walls[counter].x := i;
    Walls[counter].y := j;
    Walls[counter].dx := 0;
    Walls[counter].dy := -1;
    counter := counter + 1; 
    end;

    for i := 0 to (Width - 1) * Height + (Height - 1) * Width - 1 do 
    for j := i to (Width - 1) * Height + (Height - 1) * Width - 1 do
    if Temp[i] > Temp[j] then
    begin 
    tempr := Temp[i];
    Temp[i] := Temp[j];
     Temp[j] := tempr;
    tempw := Walls[i];
     Walls[i] := Walls[j];
     Walls[j] := tempw;
    end;

    locations := Width * Height;
    i := 0;
    while locations > 1 do
    begin
    CurWall := Walls[i]; 
    i := i + 1; 
    if not IsConnected(CurWall.x, CurWall.y, 
    CurWall.x + CurWall.dx, CurWall.y + CurWall.dy) then 
    begin 
    BreakWall(CurWall.x, CurWall.y, CurWall.dx, CurWall.dy); 
    locations := locations - 1; 
    ShowMaze(TheMaze); 
    Application.ProcessMessages; 
    end; 
    end;

    KruskalGenerateMaze := TheMaze;
end;


function Tform1.Rectangel(x,y:integer):TMarkRect;
var xc,yc:Integer;
markrect:TMarkRect;
begin
  markrect.x:=x;
  markrect.y:=y;
    xc := strtoint(form2.edt3.text) * (2 * X + 1) div 2;
    yc := strtoint(form2.edt3.text) * (2 * Y + 1) div 2;
    Form1.Screen.Canvas.Pen.Color:=clBlack;
    Form1.Screen.Canvas.Rectangle(xc - 5, yc - 5, xc + 5, yc + 5);
    result:=markrect;
end;

procedure Tform1.RectangelFree(x,y:integer);
var xc,yc:Integer;
begin
    xc := strtoint(form2.edt3.text) * (2 * X + 1) div 2;
    yc := strtoint(form2.edt3.text) * (2 * Y + 1) div 2;
    Form1.Screen.Canvas.Pen.Color:=clWhite;
    Form1.Screen.Canvas.Rectangle(xc - 5, yc - 5, xc + 5, yc + 5);
end;



function Tform1.CanGo(x, y, dx, dy : Integer) : Boolean;
begin
if dx = -1 then CanGo := not TheMaze[x, y].left_wall
  else if dx = 1 then CanGo := not TheMaze[x + 1, y].left_wall
  else if dy = -1 then CanGo := not TheMaze[x, y].up_wall
  else CanGo := not TheMaze[x, y + 1].up_wall;
end;

function TForm1.MoveRect(dx,dy:integer;markrect:TMarkRect):TMarkRect;
var
width,height:Integer;
begin
  Width := High(TheMaze);
Height := High(TheMaze[0]);
  finishx:=Width-1;
  finishy:=Height-1;
 if CanGo(markrect.x, markrect.Y, dx, dy) then
  begin
  RectangelFree(markrect.x,markrect.y);
  markrectangel:=Rectangel(markrect.x+dx,markrect.y+dy);
  MoveRect:=markrectangel;
 { markerUsers[markrectangel.x,markrectangel.y]:=k;   }

    if  (markrectangel.x=finishx) and (markrectangel.y=finishy) then Begin
    ShowMessage('Game Ower!!!');
    ShowMaze(TheMaze);
    NewGame;
    end;

  end;

end;

procedure Tform1.NewGame;
VAR x,y,xc,yc,Heightv, Widthv:Integer;
begin
k:=1;
Widthv := High(TheMaze);
Heightv := High(TheMaze[0]);
SetLength(markerUsers,Width-1,Height-1);
RectangelFree(markrectangel.x,markrectangel.y);
Markrectangel:=Rectangel(0,0);
//������� ��������
with Form1.Screen.Canvas do
  begin
   xc := strtoint(form2.edt3.text) * (2 * (strtoint(form2.edit1.text)-1) + 1) div 2;
   yc := strtoint(form2.edt3.text) * (2 * (strtoint(form2.edit2.text)-1) + 1) div 2;
  Pen.Color:=clRed;
  Brush.Color:=clRed;
  Ellipse(xc - 5, yc - 5, xc + 5, yc + 5);
  Brush.Color:=clWindow;
  Pen.Color:=clBlack;
  end;

markerUsers[0,0]:=1;
if (playerCount >= 2) then  begin
for x:=0 to  widthv-1 do
for y:=0 to heightv-1 do
if (((x<=0) and (y mod 3 = 0)) or ((y<=0) and (x mod 2 = 0))) and (k<=playerCount)  then begin
markerUsers[x,y]:=k;
Markrectangel:=Rectangel(x,y);
 k:=k+1;
end;
end;
end;


procedure TForm1.Button1Click(Sender: TObject);
begin
if dlgOpen1.Execute then
LoadMaze (TheMaze, dlgOpen1.FileName);
ShowMaze(TheMaze);
NewGame;
N11.Enabled:=True;
N18.Enabled:=True;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
N7.Checked:=True;
N8.Checked:=False;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  Height, Width : Integer;
begin
N5.Checked:=True;
N6.Checked:=False;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  Height, Width : Integer;
begin
N5.Checked:=False;
N6.Checked:=True;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  N7.Checked:=False;
N8.Checked:=True;
end;


procedure TForm1.Button6Click(Sender: TObject);
begin
if N7.Checked=True then
TheMaze:=PrimGenerateMaze(StrToInt(Form2.edit1.text),strtoint(Form2.edit2.text))
else
TheMaze:=KruskalGenerateMaze(StrToInt(Form2.edit1.text),strtoint(Form2.edit2.text));
NewGame;
N11.Enabled:=True;
N18.Enabled:=True;
Form1.SetFocus;
 //Form1.TabStop:=false;
end;

procedure TForm1.bottom(Sender: TObject);
var kor:TMarkRect;  //8
begin
kor:=MoveRect(0,1,Markrectangel);
end;

procedure TForm1.right(Sender: TObject); 
var kor:TMarkRect;            //10
begin
kor:=MoveRect(1,0,Markrectangel);
end;

procedure TForm1.left(Sender: TObject);
var kor:TMarkRect;          //9
begin
kor:=MoveRect(-1,0,Markrectangel);
end;

procedure TForm1.top(Sender: TObject);
var kor:TMarkRect;     //11
begin
kor:=MoveRect(0,-1,Markrectangel);
end;

procedure TForm1.ScreenMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  
begin
x:=x div strtoint(form2.edt3.text);
y:=y div strtoint(form2.edt3.text);

if markerUsers[x,y]>0 then
begin
markrectangel.x:=x;
markrectangel.y:=y;
end;
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#52: left(sender);  //65
#56: top(Sender);
#54: right(Sender);   //68
#53: bottom(Sender);
end;
end;


procedure TForm1.N14Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.N18Click(Sender: TObject);
var widthv,heightv:integer;
begin
Widthv := High(TheMaze);
Heightv := High(TheMaze[0]);
if N5.Checked=true then
WaveTracingSolve(TheMaze,markrectangel.x,markrectangel.y,Widthv-1,Heightv-1)
else
RecursiveSolve(TheMaze,markrectangel.x,markrectangel.y,Widthv-1,Heightv-1);
end;

procedure TForm1.N16Click(Sender: TObject);
begin
Form2.Show;
end;

procedure TForm1.N11Click(Sender: TObject);
begin
if dlgSave1.Execute then
SaveMaze(TheMaze,dlgSave1.FileName);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
BackBuffer.Enabled:=False;
Screen.Enabled:=False;
N11.Enabled:=False;
N18.Enabled:=False;
end;

procedure TForm1.N17Click(Sender: TObject);
begin
Shellexecute(form1.handle,nil, 'Maze_help.chm', nil, nil, SW_RESTORE);
end;

end.
