object Form1: TForm1
  Left = 244
  Top = 140
  Width = 684
  Height = 555
  Caption = #1051#1072#1073#1080#1088#1080#1085#1090#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object scrlbx1: TScrollBox
    Left = 0
    Top = 0
    Width = 668
    Height = 497
    Cursor = crSizeAll
    HorzScrollBar.Range = 100
    VertScrollBar.Range = 100
    Align = alClient
    AutoScroll = False
    BorderStyle = bsNone
    TabOrder = 0
    object BackBuffer: TImage
      Left = 0
      Top = 0
      Width = 668
      Height = 497
      Align = alClient
    end
    object Screen: TImage
      Left = 0
      Top = 0
      Width = 668
      Height = 497
      Cursor = crCross
      Align = alClient
      OnMouseDown = ScreenMouseDown
    end
  end
  object mm1: TMainMenu
    Left = 424
    Top = 408
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object N9: TMenuItem
        Caption = #1053#1086#1074#1072#1103' '#1080#1075#1088#1072
        OnClick = Button6Click
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object N11: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'...'
        OnClick = N11Click
      end
      object N12: TMenuItem
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100'...'
        OnClick = Button1Click
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object N14: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        OnClick = N14Click
      end
    end
    object N15: TMenuItem
      Caption = #1042#1080#1076
      object N18: TMenuItem
        Caption = #1056#1077#1096#1080#1090#1100' '#1083#1072#1073#1080#1088#1080#1085#1090
        OnClick = N18Click
      end
      object N16: TMenuItem
        Caption = #1085#1072#1089#1090#1088#1086#1081#1082#1080'...'
        OnClick = N16Click
      end
    end
    object N2: TMenuItem
      Caption = #1040#1083#1086#1075#1086#1088#1080#1090#1084#1099
      object N3: TMenuItem
        Caption = #1056#1077#1096#1077#1085#1080#1077
        object N5: TMenuItem
          Caption = #1042#1086#1083#1085#1086#1074#1072#1103' '#1090#1088#1072#1089#1089#1080#1088#1086#1074#1082#1072
          Checked = True
          OnClick = Button3Click
        end
        object N6: TMenuItem
          Caption = #1056#1077#1082#1088#1091#1089#1080#1074#1085#1099#1081' '#1086#1073#1093#1086#1076
          OnClick = Button2Click
        end
      end
      object N4: TMenuItem
        Caption = #1043#1077#1085#1077#1088#1072#1094#1080#1103
        object N7: TMenuItem
          Caption = #1040#1083#1075#1086#1088#1080#1090#1084' '#1055#1088#1080#1084#1072
          OnClick = Button4Click
        end
        object N8: TMenuItem
          Caption = #1040#1083#1075#1086#1088#1080#1090#1084' '#1050#1088#1072#1089#1082#1072#1083#1072
          Checked = True
          OnClick = Button5Click
        end
      end
    end
    object N17: TMenuItem
      Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
      OnClick = N17Click
    end
  end
  object dlgOpen1: TOpenDialog
    Left = 424
    Top = 352
  end
  object dlgSave1: TSaveDialog
    Left = 472
    Top = 352
  end
  object xpmnfst1: TXPManifest
    Left = 472
    Top = 408
  end
end
